// closure, function/method chainning/ module system 


// closure Example
// function add() {
//     let num = 10; 
//     return function() {
//         let num2 = 20;
//         let sum = num + num2;
//         console.log(sum);
//     }
// }
// var result = add();
// result();

//==========  method chainning
let obj = {
    result: 0,
    add: function(num1, num2) {
        this.result = this.result + num1 + num2;
        return this;
    },
    multiply: function(num) {
      this.result =  this.result * num;
      return this;
    }
}

const passwordValidator = (password) => {
    let pass = password + ''
    if(!!pass && pass.length>=6 && pass.length <=18) {
        return true;
    }
    else {
        return false;
    }
}
module.exports = {
  obj,
  passwordValidator,
};
// obj.add(10, 20)
// obj.result
// console.log(obj.add(10, 20).multiply(5).add(20,30).result);
