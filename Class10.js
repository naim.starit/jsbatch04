// function sum(num1, num2, num3=0) {
//     // console.log(arguments);
//     var total;
//     total = num1 + num2 + num3;
//     return total;
// }

// var n1, n2;
// n1=100;
// n2=200;
// var res = sum(n1, n2);
// console.log(res);
// var multiplication = res * 10; 
// console.log(multiplication);

//=======================================
//===========  function Declaration ========
//======================================
// function evenOrOdd(num) {
//     if(num % 2 === 0) return "Even";
//     else  return "Odd";
// }
// var res = evenOrOdd(9);
// var res1 = evenOrOdd(12);
// console.log(res, res1); // "even" // "odd"


//=======================================
//===========  function Expression ========
//======================================
// var myFunc; 
// myFunc = function hello(num1, num2) {
//     return 'Hello from hello function' + num1  + num2;
// };

// var res = myFunc(50, 55);
// console.log(res);

function add(num1, num2) {
    var total;
    total = num1 + num2;
    
    return function sum() {
        var num3 = 100;
        var result = total + num3;
        return result
    }
}

var  resFromFunc = add(50, 60);
console.log(resFromFunc);