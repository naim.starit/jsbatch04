// var myName = 'Next Topper';
// var myAge = 25;

// if(myName[0] === 'N' && myAge>18 ) {
//     console.log('Yes');
// }

// else {
//     console.log('No');
// }

// 1 - 6 -> baby
// 7 - 17 -> teenager
// 18 - 40 -> young 
// 40 - 100  -> old

// var age = 120;

// if (age >= 1 && age <= 6) {
//   console.log("baby");
// } else if (age >= 7 && age <= 17) {
//   console.log("Teenager");
// } else if (age >= 18 && age <= 40) {
//   console.log("Young");
// } else if (age >= 41 && age <= 100) {
//   console.log("Old");
// } else {
//   console.log("Invalid input");
// }

// var num = -10;

// if(num > 0) {
//     console.log('positive');
// }
// else {
//     console.log('Negative');
// }

// var age, eyeColor, weight;
// age = 15;
// eyeColor = 'black';
// weight = 85;

// if(age >= 18) {
//     if(eyeColor === 'black') {
//         if(weight >= 70 && weight <= 80) {
//             console.log('You are a real hero');
//         }
//         else {
//             console.log("You are cicked from final round");
//         }
//     }
//     else {
//         console.log("You are cicked from second round");
//     }
// }
// else {
//     console.log('You are cicked from first round');
// }

// var num = 15;
// if(num%2==0) console.log('Even');
// else console.log('Odd');  



//==========================================
//=============== Switch satement ==========
//========================================== 
var num = 2;

switch(num) {
    case 1: 
        console.log('One');
        break;
    case 2: 
        console.log('Two');
        break;
    case 3: 
        console.log('Three');
        break;
    case 4: 
        console.log('Four');
        break;
    case 5: 
        console.log('Five');
        break;
    default: 
        console.log('Invalid input');
}
