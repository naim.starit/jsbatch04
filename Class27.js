// function add(num1, num2) {
//     return num1 + num2;
// };

// function sum(cb, num3) {
//   let res = cb(20, 30) + num3;
//     console.log(res);
// };
// sum(add, 10);

// let cb = () => {
//   console.log("second callback");
// };
// setTimeout(cb, 3000);

// setTimeout(()=>{
//     console.log('hello');
// }, 2000);


// console.log("hello 1");
// setTimeout(() => {
//   console.log("hello");
// }, 0);
// console.log("hello 2");


// callback hell
// setTimeout(() => {
//   console.log("second callback");
//   setTimeout(() => {
//     console.log("hello");
//   }, 2000);
// }, 3000); 


let input = 10;
let myPromise = new Promise((resolve, reject) => {
    if(input===10) {
        resolve(`Resolve data is: ${input}`)
    }
    else {
        reject('please input valid data')
    }
});
// console.log(myPromise);
// myPromise.then(
//     (result)=>{console.log(result)},
//     (error)=>{console.log(error)}
//  );
myPromise
.then((result)=>{console.log(result)})
.catch((err)=>{console.log(err)})

