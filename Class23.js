// Type convertion part 2 
// explicit type convertion 

// to Number 

// let res = Number('56');
// let res = Number(true);
// let res = Number(false);
// let res = Number(null);
// let res = Number(''); //0
// let res = Number('dlf'); //NaN
// let res = Number(NaN); //NaN

// let num = parseInt('4565.56');
// let num = parseFloat('4565.56');


// console.log(10 + +"545");

// let res = String(655);
// let res = String(5+5);
// let res = String(undefined); // same as null, NaN, false, true

// let num = 9;
// console.log(num.toString());
// console.log(num.toString(2)); // to binary

// 0 1 2 3 4 5 6 7 

// let a = 0; // false 
// let b = 8545; // true 
// let c = '   '; // true 
// let c = ''; // false 
// let c = 'dsfdf'; // true 

// console.log(Boolean(undefined));
// console.log(Boolean(null));
// console.log(Boolean(NaN));

// falsy value 
// null, undefined, NaN, '', 0

// var arr = [45, '', '44', null, true, 0, undefined]; 

// var res = arr.filter(ele=> {
//     return Boolean(ele)  // true
// });

// console.log(res); 

// var res = 10 + 10 + '5' + +10 + 20 + '2' + Number(true);// 205102021
// var res =  Boolean(' ') + +'10' + 10 + 20;
// console.log(res);  // 'true101020'


//   Recursive Function 

// let num = 123456; // 1+2+3+4+5+6=21 

// function sumOfDigit(num) {
//     if(num==0) return 0;
//    return num%10 + sumOfDigit(parseInt(num/10))    // 6
// }

// var res = sumOfDigit(num);
// console.log(res);


// function countDown(num) {
//     if(num === 0) return;
//     console.log(num);
//     countDown(num - 1);
// };
// countDown(5);

// function fact(num) {
//     if(num === 0) return 1;
//     // console.log(num);
//     return num * fact(num - 1);
// };
// var res = fact(5);
// console.log(res);


// function sdf() {
//     return 'khdlfj'
// };
// sdf(); 


// fibonacci series 

let products = [
    {
        product: 'hp',
        model: '840',
        ram: '8GB',
        processor: 'core-i7',
    },
    {
        product: 'asus',
        model: '1440Ds',
        ram: '16GB',
        processor: 'core-i9'
    },
    {
        product: 'Mac',
        model: '9871',
        ram: '8GB',
        processor: 'core-i9'
    },
]; 

products.forEach(ele=> {
    let s = `${ele.product}-${ele.model}-${ele.ram}`;
    ele.slug = s.toLowerCase()
    delete ele.processor;
});

console.log(products);