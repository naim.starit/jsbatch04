// Regular expression part 2
// quantifires 
// +  one or more
// * zero or more

// let str = "helo 4554+ everyone, 45547+ HeLo 455+ sir helo. Helo";
// let re = /[0-9]*/g;
// let res = str.match(re);
// console.log(res);

// let str = "helo 4554* everyone, 45547+ HeLo 455+ sir helo. Helo";
// let re = /[0-9]+\*/g;  //  \+ escape quantifire
// let res = str.match(re);
// console.log(res);


// let str = "hello 4554* everyone, 45547+ HeLo 455+ sir helo. Helo";
// let re = /^he/;  // start with he
// let res = re.test(str);
// console.log(res);

// let str = "hello 4554* everyone, 45547+ HeLo 455+ sir helo. Helo";
// let re = /lo$/;  // ends with he
// let res = re.test(str);
// console.log(res);

// let str = "hello 4554* everyone, 45547+ HeLo 455+ sir helo. Helo";
// let re = /^h.*o$/;  // start with a and ends with o // .* any char zero or more times
// let res = re.test(str);
// console.log(res);

// let str = "grrreen";
// let re = /^g[a-z]{3}n$/;  
// let res = re.test(str);
// console.log(res);

// let str = "hello sdkfj154 546554 aaaaaaaa 54 kdire aaaa ie aa aaa kei";
// let re = /[a]{4,}/g;  
// let res = str.match(re)
// console.log(res);

// let words = str.split(' ');
// let regExp = /^a{4}$/;
// let res = words.filter(ele=> regExp.test(ele));
// console.log(res);

// let str = "he -5714 llo +454 kfj 154 546 dfdf 554 aaa -54 kdire kei";
// let re = /(\+|-)?[0-9]+/g;  
// let res = str.match(re)
// console.log(res);

// let str = "he abcdkei -5714 llo +454 kfj abcabcabc 154 546 dfdf 554 abbbacca -54 kdire kei";
// let re = /(abc)+/g;
// let res = str.match(re) 
// console.log(res);

// let pass = "4545edfh&%54"


// 45 47.23  7884.14545845   0.54455

//====================================
// meta character
// [a-zA-Z0-9] -> [\w] 
// [\W] oposite of [\w]
// [\d] -> [0-9] 
// [\D] -> oposite of [\d]


// let str = "he abcdkei *& -5714 llo +454 kfj abca*/ & @bcabc 154 546 dfdf 554 abbbacca -54 kdire kei";
// let re = /[\W]+/g;
// let res = str.match(re) 
// console.log(res);

// let str = "hp*)(&probook%@450&g5-core+!i5-laptop";
// let re = /[\W]+/g;
// let res = str.replace(re, '-');
// console.log(res);

// +8801723557105
// 01723557105
// 01323557105
// 01423557105
// 01523557105

// var pass = 'aa$4545geFer';
// var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W]).{8,15}$/;
// var res = re.test(pass);
// console.log(res);


// task solution 
// let phoneNumber = "+8807172032678";

// var phoneNumberChecker = (phoneNumber) => {
//   let checkingRex = /^(?:\+88|88)?(01[3-9]\d{8})$/;
//   let tester = checkingRex.test(phoneNumber);
//   if (tester) {
//     console.log("It's a BD phone number");
//   } else {
//     console.log(
//       "It's not a BD phone number, Please input a valid phone number!!!"
//     );
//   }
// };

// phoneNumberChecker(phoneNumber);  

// var str = "dsfhj 45 sdfd 12.5445  0.544754 sdfdf";

// var pass = 'abc1A23dfj*kf';
// var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W]).{8,15}$/;
// var res = re.test(pass);
// console.log(res);