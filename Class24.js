let obj = {
    name: "Next Topper",
    age: 1,
    implicitIntro(num) { // implicit binding 
        console.log(this.name + '   ' + this.age, "num: ", num);
    },
    explicitIntro(num1, num2, num3) {
         console.log(this.name + "   " + this.age, "num: ", num1, num2, num3);
    }
};

let obj1 = {
    name: "Hello",
    age: 5
};
// implicit binding 
// explicit binding 

obj.implicitIntro(56);
// call
// obj.explicitIntro.call(obj1, 10, 20, 30); 
// obj.explicitIntro.apply(obj1, [10, 50, 500]); 
var newFunc = obj.explicitIntro.bind(obj1, 5, 15, 25);
newFunc();


// call, apply, bind