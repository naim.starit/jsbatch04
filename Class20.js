// var obj = {
//     name: "Next",
//     age: 2,
//     section: "A",
//     // address: {
//     //     district: "Dhaka",
//     //     division: "Dhaka",
//     //     postCode: "1213"
//     // }
// };

// var obj1 = {color: "black", price: 100, ...obj};
// var obj1 = {color: "black", price: 100};

// var res = Object.assign(obj1, obj); // (target, source)
// obj.address.area = "Dhanmondi";

// console.log(obj1);

// var keys = Object.keys(obj);
// var values = Object.values(obj);
// var objArr = Object.entries(obj);

// for(var [key, value] of Object.entries(obj)) {
//     console.log(`Key: ${key}, Value: ${value}`); 
// }

// var res = obj.hasOwnProperty('age');
// Object.freeze(obj);
// obj.className = 'six';
// delete obj.age;
// console.log(obj);

// shallow copy
// var copyObj = {...obj};
// obj.className = "six";
// console.log(copyObj);

// deep copy 

// var res = JSON.stringify(obj);
// obj.address.area = "Dhanmondi";
// var copyObj = JSON.parse(res);
// console.log(copyObj.name);

// var str = "hello";
// var copyStr = str;
// str = "hi";
// console.log(copyStr);


// var res = Object.create(obj);
// console.log(res.age);



var obj = {
  name: "Next",
  age: 2,
  section: "A"
};
// Object.freeze(obj);
// console.log(Object.isFrozen(obj));
var arr = [
    ["a", 100],
    ["b", 200],
    ["c", 300]
]

var res = Object.fromEntries(arr);
console.log(res.a);