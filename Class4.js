// Arithmetic Operator 

// +, -, *, /, %, **, ++, --
// +
// var num1, num2, res; 
// num1 = 10, num2 = 5; 
// res = num1 + num2;
// console.log(res); // 15

// // -
// var num1, num2, res; 
// num1 = 10, num2 = 5; 
// res = num1 - num2;
// console.log(res); // 5

// // *
// var num1, num2, res; 
// num1 = 10, num2 = 5; 
// res = num1 * num2;
// console.log(res);  // 50

// // /
// var num1, num2, res; 
// num1 = 10, num2 = 5; 
// res = num1 / num2;
// console.log(res);  // 2

// // % modulas
// var num1, num2, res; 
// num1 = 3, num2 = 4; 
// res = num1 % num2;
// console.log(res);  // 3

// // ** exponential 
// var base, power, res; 
// base = 3, power = 4; 
// res = base ** power;
// console.log(res); // 81



// //  ++  increment operator 
// var num1;
// num1 = 10; 
// num1++; 
// num1++; 
// console.log(num1);

//  ++  decrement operator 
// var num1;
// num1 = 10; 
// num1--; 
// num1--; 
// console.log(num1);


// post increment - pre increment 
// var num = 10; 
// num++; // post increment 
// console.log(num);
// ++num; // pre increment 
// console.log(num);

//post decrement - pre decrement 
// var num = 10; 
// num--; // post decrement 
// console.log(num);
// --num; // pre decrement 
// console.log(num);






