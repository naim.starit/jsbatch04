//  Regular Expression

// let re = new RegExp("hello");
// let ree = /hello/;
//==========================================
//==========================================
// flag:
//  i. global flag (g) 
// ii. ignoreCase flag(i)

// expression 
// [a-z] [0-9] [aeiou]

// quantifires 
// +  one or more 

//===========================================
//===========================================

// let str = "helo everyone, HeLo sir helo. Helo";
// let re = /helo/gi
// // let res = str.search(re);
// let res = str.replace(re, "hello");
// console.log(res);

//========================== match with 2 digits
// let str = "helo 66 everyone,7 HeLo 87454 sir 657 helo. 93 Helo";
// let re = /[0-9][0-9]/g
// let res = str.match(re);  // [] | null
// console.log(res);

// match with digits
// let str = "helo 66 everyone,7 HeLo 87454 sir 657 helo. 93 Helo";
// let re = /[0-9]+/g
// let res = str.match(re);  // [] | null
// console.log(res);

// // match with exatly 2 digits
// let finalRes = res.filter(ele=> ele.length === 2)
// .map(ele=> +ele).reduce((a,b)=> a+b);
// console.log(finalRes);


// ========================= test ============= 
// var str = "1534";
// let re = /[0-9][A-Za-z0-9][0-9][0-9]/;
// let res = re.test(str);
// console.log(res);


// Check given number gp or not===============
//============================================
// let phone = "01923557105";
// let re = /[0][1][73][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]/;
// let isGP = re.test(phone);
// console.log(isGP);
//=======================
// let phone = "01723557105";
// let re = /^[0][1][73][0-9]{8}$/;  // {lowerLimit, upperLimit} {exactLimit} {min, }-> min to infinity
// let isGP = re.test(phone);
// console.log(isGP);