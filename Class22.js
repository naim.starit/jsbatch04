// let var const 

// console.log(num1);

// var name, age;
// age = 5;
// let num1;
// console.log(num1);

// let num1 = 100;
// function a() {
//     // let num1 = 500;
//     if(true) {
//         // let num1 = 100;
//         const num2 = 200;
//         var num3 = 300;
//         console.log(num1);
//     }
//     console.log(num1);
// };
// a();



// type conversion 

// 1. Implicit 2. Explicit 

// to string 
// var res = 2 + 5 + 8 + '3' + 2; // '1532'
// var res = '5' + true; // '5true'
// var res = '5' + undefined; // '5undefined'
// var res = '5' + null; // '5null' 


// string to number
// var res = '4' * '2';
// var res = '4' - '2';
// var res = '4' / '2';
// var res = '4' % '2';
//  var res = 'ab' - '5';  // NaN


// boolean to number 
// var res = '4' - true;  // 4 - 1
// var res = 4 + false;  // 4 + 0
// var res = true + true;  // 1 + 1
// var res = true + false;  // 1 + 0

// var res = 4 - null;   // null -> 0

// var res = 4 + undefined;
// var res = 4 - undefined;
// var res = null - undefined;
// var res = true - undefined;

// var res = true + false + undefined + 5 + 6 + "12";
// var res = '10' + 5 + 6 + 7  - 10; // '10567' - 10 
// var res = null + false * true + 10 + 20 + undefined + false;

// console.log(res);