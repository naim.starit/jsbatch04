// for loop 
// for(initialization, condition, update) {}

// while loop 
// var start = 1;
// while(start<=10){
//     console.log('hello', start);
//     start++;
// }

// var testCase = 5; 
// while(testCase--) {
//     console.log('hello', testCase);
// }


// var start = 1;
// do {
//     console.log('hello');
//     start++;
// }
// while(false);

//========================================
//================ Object ================ 

var student = {
    studentName: "Next Topper", // key: value -> property
    age: 1,
    rollNumber: 1,
    className: 'Five',
    phoneNumber: '54455485454',
    isReguler: true,
    hobbies: ['gardenning', 'reading', 'coding'],
    parentInfo: {
        fathersName: 'Mr. X',
        mothersName: 'Mrs. Y',
        phoneNumber: '46547564564'
    }
};

var { 
    studentName: myName,
    age, phoneNumber,
    hobbies,
    parentInfo: {fathersName, mothersName}  // nested destructuring
    } = student;

console.log(student.parentInfo);

var {age, rollNumber} = student;
console.log(age);
// property access
// way 1
// console.log(student.isReguler); // dot notation 
// way 2
// console.log(student['phoneNumber']);
// way 3
// var studentName = 'studentName'
// console.log(student[studentName]);


//for..in loop
// for(var key in student) {
//     console.log(student[key]);
// }

// var arr = [1, 2, 3, "jh", false];
// for (var key in arr) {
//   console.log(arr[key]);
// }

// var arr = [1, 2, 3, "jh", false];
// for (var value of arr) {
//   console.log(value);
// }