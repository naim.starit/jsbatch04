// Array methods 
// findIndex, indexOf, sort, reverse, slice, splice

// findIndex
// var arr = [ 20, 30, 32, 35, 40, 2, 5, 8, 10, 14, 52];
// var index = arr.findIndex(ele=> {
//     return ele == 100;
// });
// console.log(index);

// indexOf
// var arr = [20, 30, 32, 35, 40, 2, 5, 8, 10, 14, 52];
// var index = arr.indexOf(35, 6);  // (searchValue, startIndex)
// console.log(index);

// var isIncluded = arr.includes(40)
// console.log(isIncluded);

// var arr = [2, 30, 32, 35, 40, 2, 5, 8, 10, 14, 52];
// var sortedArr = arr.sort((a, b) => {
//     console.log(a, b);
//    return a - b;
// });
// console.log(sortedArr);

// var arrOfString = ['apple', 'cat', 'i', 'green', 'bat', 'purple'];
// var arr = [2, 30, 32, 35, 40, 2, 5, 8, 10, 14, 52];
// var res = arrOfString.sort();
// // var reverseArr = arr.reverse()
// console.log(res);

//slice
// var arr = [2, 30, 32, 35, 40, 2, 5, 8, 10, 14, 52]; 
// var newArr = arr.slice(5, 10);
// console.log(newArr, arr);


//splice 
// var arr = [2, 30, 32, 35, 40, 2, 5, 8, 10, 14, 52]; 
// insert, update, delete 
// delete element
// var res = arr.splice(2); // return deleted elements (startIndex, deleteCount)

//element insert
// arr.splice(4,0,100,200,300)

// update element
// arr.splice(2,2, 100, 200);
// console.log(arr);

// var arr = ['apple', 'banana', 'lichi', 'jackfruit'];
// var index = 2; 
// arr.splice(index, 1, "mango");
// console.log(arr);