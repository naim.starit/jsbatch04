// JavaScript execution context 
// execution context has two phases 
// 1. creation phase 
// 2. execution phase 

// let x = 10;
// function add(num) {
//     return num + 10;
// };

// let res = add(x);
// console.log(res);
// 1. declare the x variable and assign value 10 to x 
// 2. declare function add(), the function recieves an argument and 
//      return the sum of argument and 10
// 3. call the function add() with the argument 10, and store
//      return value to res variable 
//


// during creation phase ========================
//1. create a global object 
//2. create this object and bind to the global object 
//3. setup heap memory for storing variable and function reference 
//4. assing undefined to variables and store function reference to function name 



//==========================================
//============== Call Stack =================


// function multiplication(n) {
//    return n * 3
// }
// function add (num) {
//   return  add(num) + 20;
// }
// let res = add(5);
// console.log(res);


//===============================
function get_numbers(input) {
  let re = /([\d]+[.])?[\d]+/g;
  return input.match(re);
  // return input.match(/([\d]*[.][\d]*)|[\d]+/g);
}

var first_test = get_numbers("something11.02");
var second_test = get_numbers("something102or12");
var third_test = get_numbers("no numbers here!");

console.log(first_test);
console.log(second_test);
console.log(third_test);
