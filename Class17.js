//  Array methods 
// push, pop, shift, unshift, map, foreach, filter, isArray, includes

// var numbers = [12, 10, 5, 7, 9, 74, 63, 25, 14, 17];

// numbers.push(500, 200);

// numbers.pop();
// numbers.pop();
// var deletedValue = numbers.pop();


// numbers.unshift(50, 51, 52)
// var deletedValue = numbers.shift()

//============== map
// var numbers = [12, 10, 5, 7, 9, 74, 63, 25, 14, 17];

// update our main array
// numbers.map(function(element, index ) {
//     if(index == 5) {
//         numbers[index] = 1000;
//     }
// })

// return new array
// var result = numbers.map(function(element) {
//     return element * 2;
// })

// var result = numbers.map((element) => element * 2)

// console.log(result);
// console.log(numbers);

// var input = '5 3 4.6 7'
// var ar = input.split(' ').map(ele => +ele);
// var [num1, num2, num3, num4] = ar;
// var num1 = parseInt(ar[0])
// var num2 = parseInt(ar[1])
// var num3 = parseInt(ar[2])
// console.log(num3);  

// var student = {
//     name: "Next",
//     age: 5
// }

var students = [
    {
        name: "Next",
        age: 5
    },
    {
        name: "Next 1",
        age: 6
    },
    {
        name: "Next 2",
        age: 7
    },
    {
        name: "Next 1",
        age: 8
    },
];
var num = 5;
// isArray 

// var res = Array.isArray(num);
// console.log(res);

// students[1].age = 100;

// students.forEach((ele)=> {
//     if(ele.name === 'Next 1') {
//         ele.age = 100;
//     }
//     ele.className = 'Five';
// });

// console.log(students);


// filter

// var newArr = students.filter(ele=> {
//     if(ele.name === 'Next 1') return ele;
// });
// console.log(newArr);

// var numbers = [12, 10, 5, 7, 9, 50, 74, 63, 25, 14, 17];
// var filteredArr =  numbers.filter(ele=> {
//     if(ele <= 50) return ele;
// })
// console.log(filteredArr);
// var res = numbers.includes(10);  // Boolean
// var res = numbers.includes(7, 5); // false
// console.log(res);

