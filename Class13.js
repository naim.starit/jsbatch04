// for(var start=1; start<=5; start++) {
//     console.log('outside loop', start);
//     for(var i=1; i<=3; i++) {
//         console.log('inside loop', i);
//         for(var j=10; j<=12; j++) {
//             console.log('j = ', j);
//         }
//     }
// }

// *
// **
// ***
// ****
// *****
// for(var row=1; row<=5; row++) {
//     var star='';
//     for(var column=1; column<=row; column++) {
//         star += '*' // *****
//     }
//     console.log(star);
// }

// *****
// ****
// ***
// **
// *

// for (var i = 0; i <= 9; i++) {
//   var k = "";
//   for (var j = i; j <= 9; j++) k = k + "*";
//   console.log(k);
// }

// for(row=5; row>=1; row--) {
//     var star = '';
//     for(var column=1; column<=row; column++) {
//         star += "*";
//     }
//     console.log(star);
// }

// 1
// 12
// 123
// 1234
// 12345
// for(var row=1; row<=5; row++) {
//     var star='';
//     for(var column=1; column<=row; column++) {
//         star += column // *****
//     }
//     console.log(star);
// }

// for (row = 5; row >= 1; row--) {
//   var star = "";
//   for (var column = 1; column <= row; column++) {
//     star += column;
//   }
//   console.log(star);
// }

//       *
//      **
//     ***
//    ****

//===========================================
///////             ARRAY 
//===========================================


// var studentsOfClassFive = "Karim";
// var a = []
// var arr = [45, 56, 87, 1354, 7895];
// // console.log(arr[3] + arr[0], arr.length);
// arr.push(500, 600, 700, 800);
// console.log(arr[arr.length-1]);
// // var arr1 = ["blue", 50, false, null, undefined];

// var myArr =  new Array(5);
// myArr.push('hello', 1, undefined);
// myArr[0] = 500;
// myArr[3] = 900;
// console.log(myArr);
// myArr.push(56)

var arr = [45, 56, 87, 1354, 7895];
var anotherArr = [500, 600, 700]

for(var i=0; i< anotherArr.length; i++) {
   arr.push(anotherArr[i]);
};

var sum=0;
for(var i=0; i<arr.length; i++) {
    sum += arr[i]
}
arr.length = 0;
console.log(arr);
console.log(sum);