const {Worker} = require("worker_threads");

let workerData = 10;

const worker = new Worker("./b.js", {workerData});

worker.once("message", result => {
    console.log(`${workerData}th Fibonacci No: ${result}`);
});

worker.on("error", error => {
    console.log(error);
});

worker.on("exit", exitCode => {
    console.log(`It exited with code ${exitCode}`);
})

console.log("Execution in main thread");