// var arr  = [10, 52, 54, 13, 50, 60, 70, 80, 90];
// var [num1, , , , num2, num3, ...rest] = arr; // array destructuring // ...rest -> rest operator
// console.log(rest);
// // var num1 = arr[0];

// var days = ['sat', 'sun', 'mon'];
// var [day1, day2, day3] = days; 


// var arr = [10, 52, 54, 55, 13, 50, 53, 60, 70, 80, 90];
// var evenArr = [], oddArr = [];
// for(var i=0; i<arr.length; i++) {
//     if(arr[i] % 2 === 0) evenArr.push(arr[i]);
//     else oddArr.push(arr[i])
// }
// console.log(oddArr);


//======================================
//================== Function 

// function sum(a,b) {
//     return a+b;
// }
// var res = sum(10, 20);
// console.log(res);

// var add = function h(num1, num2, num3=0) {
//     return num1 + num2 + num3;
// }
// var res1 = add(25, 30);
// console.log(res1);

// function a(num1=0) {
//     return function b(num2=0) {
//         return function c(num3=0) {
//             var total = num1 + num2 + num3;
//             return total;
//         }
//     }
// };

// var res = a()(50)();
// console.log(res);

// annonymus function 

// var sum = function(a, b) {
//     return a+b;
// }; 

// sum();

// (function(a, b) {
//     console.log(a+b);
// })(5,5) 

//=============================
//       Arrow function 

// var sum = () => {
//     return a+10;
// };

// var sum = a => {
//     return a+10;
// }; 
// var sum = (a, b) => {
//     return a+b;
// }; 

// var sum = (a, b) => a+b;

// var res = sum(5,20);
// console.log(res);


// setTimeout(()=>{
//     console.log('hello');
// }, 3000)

// setTimeout(() => {
//   console.log("Bye");
// }, 2000);


var arr = [10, 52, 54, 55, 13, 50, 53, 60, 70, 80, 90];
var filteredArr = arr.filter((element)=> {
    return element%2 !== 0;
});
console.log(filteredArr);