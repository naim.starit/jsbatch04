// more Promise........
var students = [
    {
        name: "Rahim",
        age: 5
    },
    {
        name: "Karim",
        age: 6
    },
    {
        name: "Sakib",
        age: 7
    },
]
// function promiseOne() {
//     console.log('loading....');
//     return new Promise((resolve, reject)=>{
//         setTimeout(()=>{
//             resolve(students);
//         }, 2000)
//     })
// };

function wakeUp() {
    return new Promise((resolve, reject)=>{
        setTimeout(()=>{
            resolve("It's 6'o clock. please wake up");
        }, 1000)
    })
};

function teethYourBrush() {
    return new Promise((resolve, reject)=>{
        setTimeout(()=>{
            resolve("Teeth your brush");
        }, 3000)
    })
};
function takeBreakfast() {
    return new Promise((resolve, reject)=>{
        setTimeout(()=>{
            resolve("Take your breakfast");
        }, 2000)
    })
};

// normal way===========================
// function execute() {
//     wakeUp().then(res=>console.log(res))
//     teethYourBrush().then(res=>console.log(res))
//     takeBreakfast().then(res=>console.log(res))
// }

// execute();


//=============== Promise.all
// Promise.all([wakeUp(), takeBreakfast(), teethYourBrush() ])
// .then((res) =>console.log(res));

// Promise chainning...............
// wakeUp()
//   .then((res) => {
//     console.log(res);
//   })
//   .then(teethYourBrush)
//   .then((res) => console.log(res))
//   .then(takeBreakfast)
//   .then((res) => console.log(res))
//   .catch(err=>console.log(err))


//async .....  await 
async function execute() {
   try {
        let res3 = await wakeUp();
        console.log(res3);

        let res2 = await teethYourBrush();
        console.log(res2);

        let res1 = await takeBreakfast();
        console.log(res1);

        //=========================
   } catch(err) {
       console.log('something wrong');
   }
}
//  execute();
 console.log('123................');

// const fn = async () => {

// }


// try {
// console.log("hello");
// console.log(num + 2);
// } catch(error) {
//     console.log(error);
// }
// console.log('final log');
// console.log('sdlkfjdiofj');


