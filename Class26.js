class Animal {
    #car;
    constructor(name, age, color) {
        this.name = name;
        this.age = age;
        this.color = color;
        this.#car = "BMW"; // private property
    }
    greet() {
        console.log("hello", this.name, this.#car);
    }
    #intro() { // private method
        console.log(this.name, this.#car);
    }
    hello() {
        this.#intro()
    }
};

class Cat extends Animal {
  constructor(legs, name, age, color) {
    super(name, age, color);
    this.legsNumber = legs;
  }
  // static method 
  static helloStatic() {
      console.log("Hello I am static method", this.legsNumber);
  }
  walk() {
    console.log("Cat walks fast");
  }
}
 let cat = new Animal("cat", 2, "Black");

let cat1 = new Cat(4, "cat", 2, "Black");
// console.log(cat1);
 cat1.hello()

// Cat.helloStatic();
