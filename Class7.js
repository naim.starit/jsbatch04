// conditional statement 
// if else 

// var num = 15;
// if(num%2 === 0) {
//     console.log(num, 'is an even number');
// }
// else {
//     console.log(num, 'is an odd number');
// }


// var myAge = 18;

// if(myAge >= 18) {
//     console.log('You are a voter');
// }
// else {
//     console.log('You are a child');
// }


// var res = (12>10) && (10 === '10') || (5 <= 6);
// console.log(res);

// var res = (3 + 5) * 4; // 3 + 20
// console.log(res);


// var res1 = 5 < 10 > 15 == 0;
//         // = true > 15 == 0
//         // = false == 0
//         // = 0 == 0
//         // = true
// console.log(res1);

// var num = 5 * 10 / 2 % 4; 
//         // = 50 / 2 % 4;
//         // = 25 % 4; 
//         // = 1
// console.log(num);

// var num = 20;
// num += 10 * 5; // 70
// console.log(num);

// var res = (12>10) && (10 === '10') || (5 <= 6);
//         // = true && false || true
//         // = false || true
//         // = true
// console.log(res);

// var num = 24;

// // num % 3 === 0 
// // num % 10 === 3

// if (num % 3 === 0 && num % 10 === 3) {
//     console.log('yes');
// }

// else {
//     console.log('No');
// }
