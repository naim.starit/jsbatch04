// for(var start=0; start<15; start++) {
//     console.log(start);
// }

// for(var i=14; i>=0; i--) {
//     console.log(i);
// }
// i = 0;

// i+=2 // 2
// i+=2 // 4
// i+=2 // 6
// i+=2 // 8
// i+=2 // 10

// for(start=99; start>=1; start -= 2) {
//     console.log(start);
// }


// nested for loop

// for(var start=1; start<=10; start++) {
//     console.log('when start = ', start);
//     for(var nested=26; nested<=30; nested++) {
//         console.log('nested', nested);
//     }
// }

// whent start = 1,
// hello
// nested 26
// nested 27
// nested 28
// nested 29 
// nested 30

// when start =  2
// hello
// nested 26
// nested 27
// nested 28
// nested 29 
// nested 30

// var res='';
// for(var i=0; i<10; i++) {
//     res = res + i;
//     // console.log(res);
// };
// console.log(res);
// i=0, 
// res = '0'
// i=1 
// res = '0' + 1 = '01'


// var res = 0;
// for (var i = 1; i <= 5; i++) {
//   res = res + i;
// }
// console.log(res);
// 1+2+3+4+5 = 15
// when i=1, 
// res = res + i  = 0 + 1 = 1
// when i=2, 
// res = res + i = 1 +2 = 3


// for (var i = 1; i <= 5; i++) {
//  console.log('******');
// }


// function a(num1) {
//     return function b(num2, num3) {
//         return num1 + num2 + num3
//     }
// }

// var res = a(10)(20,30);
// console.log(res);

function converter(value, operation) {
    var res;
    switch (operation) {
        case "CtoF": 
            res = (value*1.8) + 32;
            break;
        case "FtoC": 
            res = (value - 32) / 1.8;
            break;
    }

    return res;
}

var result = converter(67, "FtoC");
console.log(parseInt(result));