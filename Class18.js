// var students = [
//   {
//     name: "Next",
//     age: 5,
//   },
//   {
//     name: "Next 1",
//     age: 6,
//   },
//   {
//     name: "Next 2",
//     age: 7,
//   },
//   {
//     name: "Next 1",
//     age: 8,
//   },
// ];

//  var numbers = [12, 10, 5, 7, 9, 50, 74, 63, 25, 14, 17];
//  let updatedArr = numbers.map((ele) => {
//    return ele * 2;
//  });
// console.log(updatedArr, numbers);

// var obj = {
//     name: "hello",
//     age: 10
// }
// var copyObj = {
//     ...obj  // spread operator
// }

// obj.section = "A";
// console.log(copyObj);

// var arr = [10, 20, 30];
// var copyArr = [...arr];
// arr.push(50, 60);
// console.log(copyArr);


// let updatedArr = students.map(ele=> {
//     if(ele.name === 'Next 1') {
//         ele.age = 15;
//     }
//     return ele;
// });

// students.forEach((ele, ind)=> {
//     if(ele.name === 'Next 1') {
//         ele.name = ele.name+" "+ind;
//     }
// });
// console.log(students);

// console.log(updatedArr, students);

// var num = 10;
// var copyNum = num;
// num = 20;
// console.log(num);

// var fruits = [
//   {
//     name: "Apple",
//     price: 100,
//   },
//   {
//     name: "Banana",
//     price: 50,
//   },
//   {
//     name: "Lichi",
//     price: 250,
//   },
//   {
//     name: "Orange",
//     price: 150,
//   },
// ];



// 
// var input = "2 5 6";
// var res = input.split(' ').map(ele=> +ele);
// var [a, b, c] = res;
// console.log(a, c);


// reduce 
// var total = fruits.reduce((a, b)=> {
//     return a + b.price;
// }, 0);
// console.log(total);


// var arr = [10, 20, 30, 40, 50];
//  var sum = 500;
//  for(var i=0; i<arr.length; i++) {
//      sum += arr[i]
//  }
//  console.log(sum);

// var total = arr.reduce((a, b)=> {
//     return a+b;
// }, 100);

// console.log(total);

// var arr = [10, 20, 30];
// var res = arr.join('+'); // array method
// console.log(res);
// console.log(res.split('+')); // string method

var arr = [10, 20, 30 , [45, 10, [47, 12]]];
console.log(arr.flat()); // flat (depth level) // nested arr level