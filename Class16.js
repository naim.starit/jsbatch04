// var teacher = {
//     name: "Mr. X",
//     id: '1545',
//     phoneNumber: '545646549',
//     // mobileNumber: {m: "hello"},
//     takenSubject: ['bangla', 'english', 'Math'],
//     address: {
//         division: 'Dhaka',
//         district: 'Dhaka',
//         postalCode: 1212
//     },
//     add: function(num) {
//         return num + 20;
//     },
//     hello() {
//         console.log('hello all');
//     }
// }

//====================================

// teacher.id = "1000";
// teacher.weight = '75kg';
// delete teacher.phoneNumber;
// teacher.address.division = "Rajshahi";
// teacher.takenSubject[1] = "Physics";

// console.log(teacher.mobileNumber?.m);  // optional chaining 
// var isAvailable = teacher.hasOwnProperty('mobileNumber');

// if(isAvailable) {
//     console.log(teacher.mobileNumber.m);
// }
// else {
//     console.log(teacher.mobileNumber);
// }


// var res = teacher.add(10);
// teacher.hello()


var calculator = {
    modelNum: 'fx100',
    price: 2500,
    add(num1, num2) {
        return num1 + num2;
    },
    lastDigit(num) {
        return num % 10;
    },
    hello: () => {
        console.log('hello, I am from  arrow function');
    },
    functionalities: ['add', 'sub', 'mul']
}

console.log(calculator.modelNum);

var res1 = calculator.add(20, 30);
var lastDigit = calculator.lastDigit(554645767);
console.log(lastDigit);
calculator.hello();

// var [a, b, c] = calculator.functionalities;
// console.log(a, b, c);