// constructor 

// function Student(name, age, className, section) {
//     var obj = {};
//     obj.name = name;
//     obj.age = age;
//     obj.className = className;
//     obj.section = section;
//     return obj;
// }

// var student1 = Student("Next", 10, 'Four', "A");
// var student2 = Student("Topper", 12, 'Six', "B");
// console.log(student2);

// var obj = {
//   name: "Next topper",
//   hi: function () {
//     return () => {
//       console.log("hello", this.name);
//     };
//   },
//   parentInfo: {
//     name: "Hello",
//     intro1() {
//       console.log(this.name);
//     },
//   },
//   intro() {
//     console.log(this.name);
//   },
// };
// obj.hi();

// function Student(name, age) {
//     this.name = name; 
//     this.age = age;
//     this.intro = function() {
//         console.log("hello", this.name);
//     }
// };

// var students = [
//     ["next", 5],
//     ["topper", 5],
//     ["Hello", 7],
//     ["Rahim", 6],
//     ["Karim", 5],
//     ["next", 5],
//     ["next255", 3],
// ]

// var res = students.map(ele=> {
//     return new Student(ele[0], ele[1])
// })

// console.log(res);
// res[4].intro()
// var res = new Student("next", 5);
// console.log(res.intro());

// obj.intro()



//==========================================
//==================Class===================


class Student {
    constructor(name, age) {
        this.name = name;
        this.age = age;
        this.hi = function() {
            console.log(this.age);
        }
    }
    
    hello() {
        console.log('hello', this.name);
    }
};

var student1 = new Student("Next", 10);
student1.hi();