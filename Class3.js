// variable 

// var myName = "next topper";
// var Name = "Next";
// console.log(myName);

// var num1 = 10;
// var num2 = 20;
// var num3 = 30;
// var num4 = 40;

// var num1 = 10, num2 = 20, num3 = 30; // declaration and assignment

// var num1, num2, num3; // variable decalaration 
// num1 = 50, num2 = 60;

// var studentName, rollNum, section; 
// studentName = "karim", rollNum = 10;
// section = 'a';

// var num1;
// num1 = 50; // assign
// console.log(num1);

// num1 = 100; // reassign 
// num1 = 500;
// num1 = "hello";
// console.log(num1);


// var num = 50;
// var copyOfNum; 
// copyOfNum = num;

// num = 100;
// console.log(num);


// swap two variables

// var num1, num2, temp;
// num1 = 100;
// num2 = 200; 

// temp = num1; // temp = 100
// num1 = num2;  // num1 = 200
// num2 = temp; // num2 = 100 (temp)
// console.log(num1, num2); 


// datatypes ======================= 
// number 
// string 
// boolean 
// undefined 
// null 
//==============================
// bigint 
// symbole


var age = 10; // number
var myName = "Next Topper"; // string 
var isDeleted = true;  // boolean -> true false
var num1 = null; // 
var section;  // undefined  

console.log(typeof isDeleted);












