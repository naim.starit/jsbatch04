// var num = 25.6356; 
// var res = num * 10000;
// console.log(res%10); 

// Comparison Operator 

//  == , ===, !=, !==, >, <, >=, <= 

// == 

// var num1, num2, res; 

// num1 = 30, num2 = 30; 
// res = (num1 == num2);  
// console.log(res); // true

// !=
// var num1, num2, res; 

// num1 = 25, num2 = 30; 
// res = (num1 != num2);  
// console.log(res); // true

// <
// var num1, num2, res; 

// num1 = 30, num2 = 30; 
// res = (num1 < num2);  
// console.log(res); // false

// >=
// var num1, num2, res; 

// num1 = 30, num2 = 45; 
// res = (num1 >= num2);  
// console.log(res); // false 

// <=
// var myAge, voterAge, res; 
// myAge = 25, voterAge = 18; 

// res = (voterAge <= myAge);
// console.log(res); // true  

//  ===
// var num1, num2, res; 

// num1 = 30, num2 = '30'; 
// res = (num1 == num2);  
// console.log(res); // false 


// var num = 45647; 
// var res = parseInt(num / 100) % 10;
// console.log(res);


// var num1 = 454; // integer number
// var num2 = 65.32;  // float number
// console.log(parseInt(num2));

// var num1, num2, res, firstDigit, secondDigit; 
// num1 = 4567, num2 = 8974123; 

// firstDigit = parseInt(num1 / 100) % 10;
// secondDigit = parseInt(num2 / 10000) % 10;
// sum = firstDigit + secondDigit;
// console.log(sum);



// var password, confirmPassword, res;
// password = '12345';
// confirmPassword = 12345; 

// res = (password === confirmPassword);
// console.log(res);

var password, confirmPassword, res;
password = '12345';
confirmPassword = 12345; 

res = (password !== confirmPassword);
console.log(res);

