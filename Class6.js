// assignment operator 


// += , -= , /=, *=  
// var num = 10;
// num += 5;  // num = num + 5;  // 15
// num += 5; 

// // -=
// var num = 30;
// num -= 5; // num = num - 5;  // 25
// num -= 10; // 15
// num += 5; // 20
// console.log(num);

// // /=
// var num = 30;
// num /= 5; // num = num / 5;  // 6
// num /= 2;
// console.log(num);

// *=
// var num = 30;
// num *= 5; // num = num * 5;  // 150
// num *= 2; // 300
// console.log(num);

// var num = 28; 
// num %= 5; // num = num%5 // 3
// console.log(num);


// isEven or odd? 
// var num = 20; 
// var rem = num % 2; 
// var isEven = rem === 0; 
// console.log(isEven);


// logical operator 

//  || &&  !
// 0 - false 
// 1 - true 

// var res1 = 0 || 0;
// var res2 = 0 || 1;
// var res3 = 1 || 0;
// var res4 = 1 || 1;

// var res1 = false || false;
// var res2 = false || true;
// var res3 = true || false;
// var res4 = true || true;
// console.log(res1, res2, res3, res4);


// var res1 = 0 && 0;
// var res2 = 0 && 1;
// var res3 = 1 && 0;
// var res4 = 1 && 1;

// var res1 = false && false;
// var res2 = false && true;
// var res3 = true && false;
// var res4 = true && true;
// console.log(res1, res2, res3, res4);

// var res1 = !(false && false);
// console.log(res1);

// var res3 = !(true || false);
// console.log(res3);


// string operator 

// var firstName, lastName, fullName;
// firstName = "Next";
// lastName = "Topper"; 

// fullName = firstName + ' ' + lastName;
// console.log('My name is '+firstName+ ' ' +lastName);

// var num = 10, myName = 'Next';
// var res = num + myName;
// console.log(res);  


// bitwise opearator 

// Decimal - 0,1,2,3,4,5,6,7,8,9  = 10
// Binary - 0, 1 = 2
// Octal - 0,1,2,3,4,5,6,7 = 8
// Hexadecimal - 0,1,2,3,4,5,6,7,8,9, A, B, C, D, E, F = 16